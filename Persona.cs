﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Final
{
    public abstract class Persona
    {
        public decimal DNI { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public decimal Telefono { get; set; }
        public string Direccion { get; set; }
        public Localidad Localidades { get; set; }


        public virtual string DevolverDatos ()
        {
            return $"{Apellido} {Nombre} -Telefono:{Telefono} -{Localidades.Provincia}, {Localidades.NombreCiudad}, {Direccion} ";
        }

        public abstract string DevolverFiesta();
    }
}

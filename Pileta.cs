﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Final
{
    public abstract class Pileta
    {
        public int CodigoPileta { get; set; }
        public decimal CantidadDeLitros { get; set; }
        public decimal Precio { get; set; }

        public Colores Color { get; set; }

        public enum Colores
        {
            Azul, Celeste, Gris
        }

        public abstract decimal CalcularPorcentajeDescuento();
    }
}
